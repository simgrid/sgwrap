1. Run `nix-shell -A test-shell`.
2. Create the folder `/tmp/sgwrap`
3. Now into independent shells (`nix-shell -A test-shell` in each one of them) you can run the following commands:
  1. `rsg serve tests/platforms/small_platform.xml`
  2. `rsg add-actor --no-autoconnect test Tremblay -- sgwrap-launcher -stp ./b/tests/send_file_server ./tests/network/payload`
  3. `rsg add-actor --no-autoconnect test Tremblay -- sgwrap-launcher -stTp ./b/tests/send_file_client /tmp/RFC`
  4. `rsg start`

Note: Instead of using nix-shell, you can build the various subparts of this project.
- socket_wrapper via CMake
- the python launcher via `python setup.py install`, `pip` or similar
- and all other sgwrap are managed by the same Meson build description

Then make sure that all these libraries are in your `$LD_LIBRARY_PATH`
and that you can run `rsg` and `sgwrap-launcher` (in your `$PATH` and all dependencies installed)
