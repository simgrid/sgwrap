// dl* functions
#include <dlfcn.h>
#include <stdio.h>
// RSG
#include <librsg.hpp>
#include <librsg/assert.hpp>

// Fork types (pid_t)
#include <sys/types.h>
// Fork
#include <unistd.h>
#include <execinfo.h>
#include <string.h>

// Internal function loading a symbol from the libc and returning the `fn_name`
static void *_bind_symbol(const char *fn_name) {
  void *handle;
  void *func;

  // open the libc
  handle = dlopen("libc.so.6", RTLD_LAZY);

  // find and bind the function
  func = dlsym(handle, fn_name);
  if (func == NULL) {
    // TODO print dlerror
    exit(-1);
  }
  return func;
}

// typedefs to facilitate the declaration of the libc functions
typedef int (*__libc_execv)(const char *file, char *const argv[]);
typedef int (*__libc_execve)(const char *fichier, char *const argv[],
                             char *const envp[]);
typedef pid_t (*__libc_fork)(void);
typedef pid_t (*__libc_waitpid)(pid_t pid, int *status, int options);

// global variables holding the ptr to the execv function
static __libc_execv ptrexecv;
static __libc_execve ptrexecve;
static __libc_fork ptrfork;
static __libc_waitpid ptrwaitpid;

// Macro that bind a symbol if it is not already done.
// The macro will look at a variable named ptr`fn_name`.
#define _bind_symbol_libc(sym_name)                                            \
  if (ptr##sym_name == NULL) {                                                 \
    ptr##sym_name = (__libc_##sym_name)_bind_symbol(#sym_name);                \
  }

static int libc_execv(const char *file, char *const argv[]) {
  _bind_symbol_libc(execv);
  return ptrexecv(file, argv);
}

static pid_t libc_fork(void) {
  _bind_symbol_libc(fork);
  return ptrfork();
}

static pid_t libc_waitpid(pid_t pid, int *status, int options) {
  _bind_symbol_libc(waitpid);
  return ptrwaitpid(pid, status, options);
}

static int libc_execve(const char *file, char *const argv[],
                       char *const envp[]) {
  _bind_symbol_libc(execve);
  return ptrexecve(file, argv, envp);
}

extern "C" {
pid_t fork(void) {
  RSG_INFO("Process forking");

  char *rsg_connection_fd = getenv("SGWRAP_FORKING");
  if (rsg_connection_fd == NULL){
    // disable socket_wrapper, so the child can connect to rsg
    char *initial_value = getenv("SOCKET_WRAPPER_DIR");
    char *copy = strdup(initial_value);
    unsetenv("SOCKET_WRAPPER_DIR");

    // disable fork interception
    auto host = rsg::Actor::self()->get_host();
    setenv("SGWRAP_FORKING", "true", 1);

    // Calling the RSG fork, that will create a new actor
    // INTO a new system process.
    int rsg_fork_res = rsg::Actor::fork("newborn", host);

    // Enable fork interception
    unsetenv("SGWRAP_FORKING");
    // Enable socket_wrapper interception
    setenv("SOCKET_WRAPPER_DIR", copy, 1);
    return rsg_fork_res;
  }

  // If the environment variable `SGWRAP_FORKING` is set, it means that we are intercepting
  // an internal call to fork, we call the original function from the libc.
  return libc_fork();
}

int execv(const char *file, char *const argv[]) {
  return libc_execv(file, argv);
}

int execve(const char *file, char *const argv[], char *const envp[]) {
  return libc_execve(file, argv, envp);
}

pid_t waitpid(pid_t pid, int *status, int options) {
  return libc_waitpid(pid, status, options);
}

pid_t getpid() {
  return rsg::Actor::self()->get_pid();
}
}
