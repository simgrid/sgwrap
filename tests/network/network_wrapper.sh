#!/usr/bin/env sh

SOCKET_WRAPPER_DIR="/tmp/sgwrap" \
SOCKET_WRAPPER_DEFAULT_IFACE=10 \
SOCKET_WRAPPER_DEBUGLEVEL=1 \
LD_PRELOAD="libsgwrap_init.so libtime_wrapper.so libsocket_wrapper.so libprocess.so" \
${@:1}
