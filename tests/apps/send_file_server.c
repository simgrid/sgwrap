/* Server code */
/* TODO : Modify to meet your need */
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define PORT_NUMBER 5000
#define SERVER_ADDRESS "127.0.0.10"
#define MAX_PATH_SIZE 512

int main(int argc, char **argv) {
  int server_socket;
  int peer_socket;
  socklen_t sock_len;
  ssize_t len;
  struct sockaddr_in server_addr;
  struct sockaddr_in peer_addr;
  int fd;
  int sent_bytes = 0;
  char file_size[256];
  struct stat file_stat;
  off_t offset;
  int remain_data;

  char file_to_send[MAX_PATH_SIZE];
  strcpy(file_to_send, argv[1]);

  /* Create server socket */
  server_socket = socket(AF_INET, SOCK_STREAM, 0);
  if (server_socket == -1) {
    fprintf(stderr, "Error creating socket --> %s", strerror(errno));

    exit(EXIT_FAILURE);
  }

  /* Zeroing server_addr struct */
  memset(&server_addr, 0, sizeof(server_addr));
  /* Construct server_addr struct */
  server_addr.sin_family = AF_INET;
  inet_pton(AF_INET, SERVER_ADDRESS, &(server_addr.sin_addr));
  server_addr.sin_port = htons(PORT_NUMBER);

  /* Bind */
  if ((bind(server_socket, (struct sockaddr *)&server_addr,
          sizeof(struct sockaddr))) == -1) {
    fprintf(stderr, "Error on bind --> %s\n", strerror(errno));

    exit(EXIT_FAILURE);
  }

  /* Listening to incoming connections */
  if ((listen(server_socket, 5)) == -1) {
    fprintf(stderr, "Error on listen --> %s\n", strerror(errno));

    exit(EXIT_FAILURE);
  }

  fd = open(file_to_send, O_RDONLY);
  if (fd == -1) {
    fprintf(stderr, "Error opening file --> %s\n", strerror(errno));

    exit(EXIT_FAILURE);
  }

  /* Get file stats */
  if (fstat(fd, &file_stat) < 0) {
    fprintf(stderr, "Error fstat --> %s\n", strerror(errno));

    exit(EXIT_FAILURE);
  }

  fprintf(stdout, "File Size: %d bytes\n", file_stat.st_size);

  sock_len = sizeof(struct sockaddr_in);
  /* Accepting incoming peers */
  peer_socket = accept(server_socket, (struct sockaddr *)&peer_addr, &sock_len);
  if (peer_socket == -1) {
    fprintf(stderr, "Error on accept --> %s", strerror(errno));

    exit(EXIT_FAILURE);
  }
  fprintf(stdout, "Accept peer --> %s\n", inet_ntoa(peer_addr.sin_addr));

  sprintf(file_size, "%d", file_stat.st_size);

  /* Sending file size */
  len = send(peer_socket, file_size, strlen(file_size) + 1, 0);
  if (len < 0) {
    fprintf(stderr, "Error on sending greetings --> %s", strerror(errno));

    exit(EXIT_FAILURE);
  }

  fprintf(stdout, "Server sent %d bytes for the size\n", len);

  offset = 0;
  remain_data = file_stat.st_size;
  printf("remain: %d\n", remain_data);
  char buffer[512];
  /* Sending file data */
  while (1) {
    // Read data into buffer.  We may not have enough to fill up buffer, so we
    // store how many bytes were actually read in bytes_read.
    int bytes_read = read(fd, buffer, sizeof(buffer));
    if (bytes_read == 0) // We're done reading from the file
      break;
    if (bytes_read < 0) {
      // handle errors
    }

    // You need a loop for the write, because not all of the data may be written
    // in one call; write will return how many bytes were written. p keeps
    // track of where in the buffer we are, while we decrement bytes_read
    // to keep track of how many bytes are left to write.
    void *p = buffer;
    while (bytes_read > 0) {
      int bytes_written = write(peer_socket, p, bytes_read);
      if (bytes_written <= 0) {
        // handle errors
      }
      bytes_read -= bytes_written;
      p += bytes_written;
    }

    remain_data -= bytes_read;
  }

  if (remain_data > 0) {
    printf("Exited but remain %d to transfert\n", remain_data);
  }

  close(peer_socket);
  close(server_socket);

  return 0;
}
