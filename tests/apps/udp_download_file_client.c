// Client side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>

#define PORT     8080
#define MAXLINE  256
#define MAX_PATH_SIZE 512

// Driver code
int main(int argc, char **argv) {
  int sockfd, file_size;
  char buffer[MAXLINE];
  char *hello = "It's me";
  FILE *received_file;
  char file_name[MAX_PATH_SIZE];
  struct sockaddr_in     servaddr;

  // file path
  strcpy(file_name, argv[1]);

  // Creating socket file descriptor
  if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
      perror("socket creation failed");
      exit(EXIT_FAILURE);
  }

  memset(&servaddr, 0, sizeof(servaddr));

  // Filling server information
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(PORT);
  servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  int n, len;

  sendto(sockfd, (const char *)hello, strlen(hello),
      MSG_CONFIRM, (const struct sockaddr *) &servaddr,
          sizeof(servaddr));

  printf("Hello message sent.\n");

  n = recvfrom(sockfd, (char *)buffer, MAXLINE,
              MSG_WAITALL, (struct sockaddr *) &servaddr,
              &len);

  file_size = atoi(buffer);
  fprintf(stdout, "\n(size:%d)File size : %d\n", n, file_size);

  received_file = fopen(file_name, "w");
  if (received_file == NULL) {
    fprintf(stderr, "Failed to open file foo --> %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  unsigned int remain_data = file_size;

  while ((remain_data > 0) &&
         ((len = recvfrom(sockfd, (char *)buffer, MAXLINE, MSG_WAITALL, (struct sockaddr *) &servaddr, &len)) > 0)) {

    fwrite(buffer, sizeof(char), len, received_file);
    fflush(received_file);
    remain_data -= len;
    fprintf(stdout, "Receive %d bytes and we hope :- %d bytes\n", len,
            remain_data);
  }

  printf("quit while %d remaining data (len=%d)\n", remain_data, len);
  fclose(received_file);
  close(sockfd);

  // close(sockfd);
  return 0;
}
