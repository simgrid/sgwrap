#include <stdio.h>     // for fprintf()
#include <unistd.h>    // for close(), read()
#include <sys/epoll.h> // for epoll_create1(), epoll_ctl(), struct epoll_event
#include <string.h>    // for strncmp

#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<sys/time.h>
#include<arpa/inet.h> //inet_addr
#include<pthread.h> //for threading , link with lpthread
#include<string.h> // correct header

#define UNUSED(x) (void)(x)

int main()
{
  #define READ_SIZE 10
  #define MAX_EVENTS 10
  struct epoll_event ev, events[MAX_EVENTS];
  int listen_sock, conn_sock, nfds, epollfd;
  struct sockaddr_in server, addr;
  socklen_t addrlen;

  //Create socket
  listen_sock = socket(AF_INET , SOCK_STREAM , 0);
  if (listen_sock == -1)
  {
      printf("Could not create socket\n");
  }

  //Prepare the sockaddr_in structure
  server.sin_family = AF_INET;
  server.sin_addr.s_addr =  inet_addr("127.0.0.1");

  //We will accept incoming connections from the port 8888/
  server.sin_port = htons(8080);

  if( bind(listen_sock,(struct sockaddr *)&server , sizeof(server)) < 0)
  {
      //print the error message
      perror("bind failed. Error\n");
      return 1;
  }
  //Listen
  listen(listen_sock , 3);

  epollfd = epoll_create1(0);
  if (epollfd == -1) {
      perror("epoll_create1");
      exit(EXIT_FAILURE);
  }

  ev.events = EPOLLIN;
  ev.data.fd = listen_sock;
  if (epoll_ctl(epollfd, EPOLL_CTL_ADD, listen_sock, &ev) == -1) {
      perror("epoll_ctl: listen_sock");
      exit(EXIT_FAILURE);
  }

  for (;;) {
      nfds = epoll_wait(epollfd, events, MAX_EVENTS, -1);
      if (nfds == -1) {
          perror("epoll_wait");
          exit(EXIT_FAILURE);
      }

      for (int n = 0; n < nfds; ++n) {
          if (events[n].data.fd == listen_sock) {
              conn_sock = accept(listen_sock,
                                 (struct sockaddr *) &addr, &addrlen);
              if (conn_sock == -1) {
                  perror("accept");
                  exit(EXIT_FAILURE);
              }
              // setnonblocking(conn_sock);
              ev.events = EPOLLIN | EPOLLET;
              ev.data.fd = conn_sock;
              if (epoll_ctl(epollfd, EPOLL_CTL_ADD, conn_sock,
                          &ev) == -1) {
                  perror("epoll_ctl: conn_sock");
                  exit(EXIT_FAILURE);
              }
          } else {
            printf("Acitivity on %d\n", events[n].data.fd);
          }
      }
  }
  return 0;
}
