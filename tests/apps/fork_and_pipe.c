// C program to demonstrate use of fork() and pipe()
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>
#include<sys/wait.h>

int main()
{
    // We use two pipes
    // First pipe to send input string from parent
    // Second pipe to send concatenated string from child

    int fd1[2];  // Used to store two ends of first pipe
    int fd2[2];  // Used to store two ends of second pipe

    pid_t p;

    if (pipe(fd1)==-1)
    {
        fprintf(stderr, "Pipe Failed" );
        return 1;
    }
    if (pipe(fd2)==-1)
    {
        fprintf(stderr, "Pipe Failed" );
        return 1;
    }

    p = fork();

    if (p < 0)
    {
        fprintf(stderr, "fork Failed" );
        return 1;
    }

    // Parent process
    else if (p > 0)
    {
        // close(fd1[0]);  // Close reading end of first pipe
        char buffer[100], dowey[] = "Je ne m'attendais à rien mais je suis quand même déçu";

        // Write input string and close writing end of first
        // pipe.
        int err;
        err = write(fd1[1], dowey, strlen(dowey)+1);
        close(fd1[1]);

        // sleep(42);

        // close(fd2[1]); // Close writing end of second pipe

        err = read(fd2[0], buffer, 100);
        // close(fd2[0]);
    }

    // child process
    else
    {
        // sgwrap does not handles close
        // close(fd1[1]);  // Close writing end of first pipe

        // sleep(21);

        // Read a string using first pipe
        char message[100];
        int err;
        err = read(fd1[0], message, 100);

        printf("%s\n", message);

        // Close both reading ends
        // close(fd1[0]);
        // close(fd2[0]);

        // Write concatenated string and close writing end
        err = write(fd2[1], message, strlen(message)+1);
        // close(fd2[1]);

        exit(0);
    }
}
