/*
C socket server example, handles multiple clients using threads
*/
#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<sys/time.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h> //for threading , link with lpthread
#include<string.h> // correct header
#include<unistd.h>
#define UNUSED(x) (void)(x)

//the thread function
void *connection_handler(void *);

int main(int argc , char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);


    int socket_desc , client_sock , c , *new_sock;
    struct sockaddr_in server, client;

    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket\n");
    }

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr =  inet_addr("127.0.0.10");

    //We will accept incoming connections from the port 8888/
    server.sin_port = htons( 8080 );

    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error\n");
        return 1;
    }
    //Listen
    listen(socket_desc , 3);

    //Accept and incoming connection
    c = sizeof(struct sockaddr_in);

    // The server loop.
    client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
    if (client_sock < 0)
    {
        perror("accept failed");
        return 1;
    }

    new_sock = malloc(1);
    *new_sock = client_sock;
    connection_handler((void*) new_sock);
    free(new_sock);
    return 0;
}

/*
 * This will handle connection for each client
 */
void *connection_handler(void *socket_desc)
{
    //Get the socket descriptor
    int sock = *(int*)socket_desc;
    int read_size;
    char client_message[2000];

    do {
        //Receive a message from client
        memset(client_message, 0, strlen(client_message) + 1);
        read_size = recv(sock , client_message , 2000 , 0);
        if(client_message[0] == 'q') {
            //Free the socket pointer
            printf("End of connection char received\n");
            break;
        }

        printf("client message : %s\n", client_message);

        if(read_size == 0)
        {
            printf("Client disconnected\n");
            fflush(stdout);
        }
        else if(read_size == -1)
        {
            printf("Client disconnected\n");
            perror("recv failed\n");
        }

        //Send the message back to client
        send(sock , client_message , strlen(client_message) + 1, 0);

    } while(1);
    return 0;
}
