/*
C ECHO client example using sockets
*/
#include<stdio.h> //printf
#include<string.h>    //strlen
#include<sys/socket.h>    //socket
#include<arpa/inet.h> //inet_addr
#include<string.h> // correct header
#include<unistd.h>

#define UNUSED(x) (void)(x)

int main(int argc , char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);
    int sock;
    struct sockaddr_in server;
    char server_reply[2000];

    char akadoc[] = "Tatan, elle fait des flans.";

    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket\n");
    }
    printf("start client: fd=%d\n", sock);

    server.sin_addr.s_addr = inet_addr("127.0.0.10");
    server.sin_family = AF_INET;
    server.sin_port = htons( 8080 );

    sleep(1);

    //Connect to remote server
    if((connect(sock, (struct sockaddr *)&server , sizeof(server)) < 0))
    {
        perror("waiting for connection");
        return -1;
    }

    //Send some data
    if(send(sock , akadoc, strlen(akadoc) + 1 , 0) < 0)
    {
        printf("client test:Send failed\n");
        return 1;
    }

    //Receive a reply from the server
    if(recv(sock , server_reply , 2000 , 0) < 0)
    {
        printf("recv failed\n");
        return 1;
    }

    char end_char = 'q';
    //Send some data
    if(send(sock, &end_char, 2, 0) < 0)
    {
        printf("Send failed\n");
        return 1;
    }
    close(sock);
    return 0;
}
