#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>  //Header file for sleep(). man 3 sleep for details.
#include <pthread.h>

// A normal C function that is executed as a thread
// when its name is specified in pthread_create()
void *myThreadFun(void *vargp)
{
    int *time = (int*) vargp;
    printf("Hello from thread (will sleep for %d)\n", *time);
    sleep(*time);
    printf("Thread awake %d sec later\n", *time);
    return NULL;
}

void *thread_launcher(void *vargp) {
    pthread_t thread_id;
    int *nb_thread = (int*) vargp;
    printf("Hello from launcher\n");
    for(int i = 0; i < *nb_thread; i++) {
      pthread_create(&thread_id, NULL, myThreadFun, nb_thread);
    }
    sleep(*nb_thread+1);
    free(nb_thread);
    printf("launcher exited\n");
    return NULL;
}

int main() {
    pthread_t thread_id;
    printf("Hello from main\n");

    for(int i = 1; i <= 2; i++) {
      int *nb_th = malloc(sizeof(int));
      *nb_th = 2;
      pthread_create(&thread_id, NULL, thread_launcher, nb_th);
    }

    printf("main will sleep\n");
    sleep(12);
    printf("Main exited\n");
    return 0;
}
