/* Client code */
/* TODO : Modify to meet your need */
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define PORT_NUMBER 5000
#define SERVER_ADDRESS "127.0.0.10"
#define MAX_PATH_SIZE 512
#define BUFSIZ  256

int main(int argc, char **argv) {
  int client_socket;
  ssize_t len;
  struct sockaddr_in remote_addr;
  char buffer[BUFSIZ];
  int file_size;
  FILE *received_file;
  int remain_data = 0;

  char file_name[MAX_PATH_SIZE];
  strcpy(file_name, argv[1]);

  /* Zeroing remote_addr struct */
  memset(&remote_addr, 0, sizeof(remote_addr));

  /* Construct remote_addr struct */
  remote_addr.sin_family = AF_INET;
  inet_pton(AF_INET, SERVER_ADDRESS, &(remote_addr.sin_addr));
  remote_addr.sin_port = htons(PORT_NUMBER);

  /* Create client socket */
  client_socket = socket(AF_INET, SOCK_STREAM, 0);
  if (client_socket == -1) {
    fprintf(stderr, "Error creating socket --> %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  /* Connect to the server */
  if (connect(client_socket, (struct sockaddr *)&remote_addr,
              sizeof(struct sockaddr)) == -1) {
    fprintf(stderr, "Error on connect --> %s\n", strerror(errno));

    exit(EXIT_FAILURE);
  }

  /* Receiving file size */
  recv(client_socket, buffer, BUFSIZ, 0);
  file_size = atoi(buffer);
  fprintf(stdout, "\nFile size : %d\n", file_size);

  received_file = fopen(file_name, "w");
  if (received_file == NULL) {
    fprintf(stderr, "Failed to open file foo --> %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  remain_data = file_size;

  while ((remain_data > 0) &&
         ((len = recv(client_socket, buffer, BUFSIZ, 0)) > 0)) {

    fwrite(buffer, sizeof(char), len, received_file);
    fflush(received_file);
    remain_data -= len;
    fprintf(stdout, "Receive %d bytes and we hope :- %d bytes\n", len,
            remain_data);
  }

  printf("quit while %d remaining data\n", remain_data);
  fclose(received_file);
  close(client_socket);

  return 0;
}
