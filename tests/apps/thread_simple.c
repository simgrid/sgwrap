#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>  //Header file for sleep(). man 3 sleep for details.
#include <pthread.h>

// A normal C function that is executed as a thread
// when its name is specified in pthread_create()
void *myThreadFun(void *vargp)
{
    int *time = (int*) vargp;
    printf("Hello from thread\n");
    sleep(*time);
    printf("Thread awake %d sec later\n", *time);
    free(time);
    return NULL;
}

int main()
{
    pthread_t thread_id;
    int *time = malloc(sizeof(int));
    *time = 1;

    printf("Starting new thread\n");
    pthread_create(&thread_id, NULL, myThreadFun, time);
    // pthread_join(thread_id, NULL);

    sleep(3);
    exit(0);
}

