#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define PORT_NUMBER 8080
#define SERVER_ADDRESS "127.0.0.1"
#define MAX_PATH_SIZE 512
#define MAXLINE 512

int main(int argc, char **argv) {
  int server_socket;
  socklen_t addrlen;
  struct sockaddr_in servaddr, cliaddr;
  int fd;
  char file_size[256];
  struct stat file_stat;
  int remain_data;

  char buffer[MAXLINE];
  char file_to_send[MAX_PATH_SIZE];
  strcpy(file_to_send, argv[1]);

  /* Create server socket */
  server_socket = socket(AF_INET, SOCK_DGRAM, 0);
  if (server_socket == -1) {
    fprintf(stderr, "Error creating socket --> %s", strerror(errno));

    exit(EXIT_FAILURE);
  }

  memset(&servaddr, 0, sizeof(servaddr));
  memset(&cliaddr, 0, sizeof(cliaddr));

    // Filling server information
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(PORT_NUMBER);
  servaddr.sin_addr.s_addr = inet_addr(SERVER_ADDRESS);

  /* Bind */
  if ((bind(server_socket, (struct sockaddr *)&servaddr,
          sizeof(struct sockaddr))) == -1) {
    fprintf(stderr, "Error on bind --> %s\n", strerror(errno));

    exit(EXIT_FAILURE);
  }

  fd = open(file_to_send, O_RDONLY);
  if (fd == -1) {
    fprintf(stderr, "Error opening file --> %s\n", strerror(errno));

    exit(EXIT_FAILURE);
  }

  /* Get file stats */
  if (fstat(fd, &file_stat) < 0) {
    fprintf(stderr, "Error fstat --> %s\n", strerror(errno));

    exit(EXIT_FAILURE);
  }

  fprintf(stdout, "File Size: %ld bytes\n", file_stat.st_size);

  /* Accepting incoming peers */
  int n = recvfrom(server_socket, (char *)buffer, MAXLINE,
              MSG_WAITALL, ( struct sockaddr *) &cliaddr,
              &addrlen);

  printf("Client : %s\n", buffer);

  fprintf(stdout, "Accept peer --> %s\n", inet_ntoa(cliaddr.sin_addr));

  sprintf(file_size, "%ld", file_stat.st_size);

  /* Sending file size */
  int len = sendto(server_socket, (const char *)file_size, sizeof(file_size),
      MSG_CONFIRM, (const struct sockaddr *) &cliaddr,
          addrlen);
  if (len < 0) {
    fprintf(stderr, "Error on sending greetings --> %s", strerror(errno));

    exit(EXIT_FAILURE);
  }

  fprintf(stdout, "Server sent %d bytes for the size\n", len);

  remain_data = file_stat.st_size;
  printf("remain: %d\n", remain_data);
  /* Sending file data */
  while (1) {
    // Read data into buffer.  We may not have enough to fill up buffer, so we
    // store how many bytes were actually read in bytes_read.
    int bytes_read = read(fd, buffer, sizeof(buffer));
    if (bytes_read == 0) // We're done reading from the file
      break;
    if (bytes_read < 0) {
      // handle errors
    }

    // You need a loop for the write, because not all of the data may be written
    // in one call; write will return how many bytes were written. p keeps
    // track of where in the buffer we are, while we decrement bytes_read
    // to keep track of how many bytes are left to write.
    void *p = buffer;
    while (bytes_read > 0) {
      // int bytes_written = write(peer_socket, p, bytes_read);
      int bytes_written = sendto(server_socket, (const char *)p, bytes_read,
                MSG_CONFIRM, (const struct sockaddr *) &cliaddr,
                          addrlen);

      if (bytes_written <= 0) {
        // handle errors
      }
      bytes_read -= bytes_written;
      p += bytes_written;
    }

    remain_data -= bytes_read;
  }

  if (remain_data > 0) {
    printf("Exited but remain %d to transfert\n", remain_data);
  }
  close(server_socket);
  return 0;
}
