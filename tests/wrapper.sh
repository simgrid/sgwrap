#!/usr/bin/env bash

# Wrapper to run RSG with socket wrapper.
# To activate logs from socket_wrapper you can set the variable `SOCKET_WRAPPER_DEBUGLEVEL=3`.
# The first argument should be a space separated list of libraries from socket_wrapper to use:
# Example: "build/sgwrap_init/libsgwrap_init.so socket_wrapper/build/src/libsocket_wrapper.so.0.1.13"
# The remaining parameters are the program to intercept with its parameters.

set -x
echo SOCKET_WRAPPER_DIR=/tmp/sgwrap \
SOCKET_WRAPPER_DEFAULT_IFACE=10 \
SOCKET_WRAPPER_DEBUGLEVEL=3 \
LD_PRELOAD=$1 \
"${@:2}"

SOCKET_WRAPPER_DIR=/tmp/sgwrap \
SOCKET_WRAPPER_DEFAULT_IFACE=10 \
SOCKET_WRAPPER_DEBUGLEVEL=3 \
LD_PRELOAD=$1 \
"${@:2}"

# Uncomment to activate socket_wrapper logs
