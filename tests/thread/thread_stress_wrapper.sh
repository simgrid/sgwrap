#!/usr/bin/env sh

# We also set the network interception, so we test that network is not broken when intercepted
# export LD_PRELOAD="libsgwrap_init.so libtime_wrapper.so libthread.so libsocket_wrapper.so"
export LD_PRELOAD="libsgwrap_init.so libtime_wrapper.so libthread.so"
thread_stress
