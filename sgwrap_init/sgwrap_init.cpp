#include <stdlib.h>
#include <string.h>

#include <librsg.hpp>

static void autoconnect(void) __attribute__((constructor));
void autoconnect(void)
{
  // In case of the intercepted process uses execv,
  // the stack looses every information about the current connection.
  // To be able to retrieve a connection to a connected actor,
  // One sets the environment variable: RSG_CONNECTION_FD, containing the fd of the rsg connection.
  // This first `getenv` test wether we already have a connected fd.
  char *rsg_connection_fd = getenv("RSG_CONNECTION_FD");
  if (rsg_connection_fd != nullptr)
  {
    int fd = std::stoi (rsg_connection_fd, 0);
    rsg::reuse_connected_socket(fd);
    return;
  }

  // This is specific to socket_wrapper
  // In order to not intercept the rsg messages, we temporary unset the SOCKET_WRAPPER_DIR
  // to disable the interception. Once connected, we re-set the variable te enable the interception.
  // Note that this part is specific for the libsocket_wrapper.so.
  char *initial_value = getenv("SOCKET_WRAPPER_DIR");
  if (initial_value != nullptr)
  {
    char *copy = strdup(initial_value);
    unsetenv("SOCKET_WRAPPER_DIR");

    rsg::connect();

    setenv("SOCKET_WRAPPER_DIR", copy, 1);
    free(copy);
  }
  else
  {
    rsg::connect();
  }

  // Once we are connected, we can set the `RSG_CONNECTION_FD`
  // in case of mutant process (explained at the start of this function).
  int fd = rsg::connection->socket_fd();
  std::string s = std::to_string(fd);
  setenv("RSG_CONNECTION_FD", s.c_str(), 1);
}
