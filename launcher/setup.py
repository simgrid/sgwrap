#!/usr/bin/env python
from distutils.core import setup

setup(name='sgwrap-launcher',
    version='0.1.0',
    py_modules=['sgwrap_launcher'],
    scripts=['sgwrap-launcher'],

    python_requires='>=3.6',
    install_requires=[
        'docopt>=0.6.2',
    ],

    description="Script / library to launch processes in SGWRAP.",
    author='Adrien Faure, Millian Poquet',
    author_email='adrien.faure@protonmail.com, millian.poquet@gmail.com',
    url='https://framagit.org/simgrid/sgwrap/',
    license='BSD 3-clause License',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
    ],
)
