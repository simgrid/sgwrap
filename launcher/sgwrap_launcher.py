#!/usr/bin/env python

"""sgwrap launcher.

Usage:
   sgwrap-launcher [-stTp] [--socket --process --thread --time]
   [-D=<debug-level> -i=<iface> -d=<sw-dir>] [--]
   <command-line>...

Options:
  -h --help                 Show this screen.
  --version                 Show version.
  -s --socket               Activate socket wrapper
  -p --process              Activate process wrapper
  -t --time                 Activate time wrapper
  -T --thread               Activate thread wrapper
  -d --sw-dir=<sw_dir>      SW socket dir [default: /tmp/sgwrap]
  -i --sw-iface=<sw_iface>  SW interface [default: 10]
  -D --sw-dbg-lvl=<sw_dgb>  SW debug level [default: 0]
"""

import glob
import os
import subprocess
import tempfile
from docopt import docopt

# Associates sgwrap libraries names to .so filenames
SGWRAP_LIBS = {
    'init': "libsgwrap_init.so",
    'socket': "libsocket_wrapper.so",
    'thread': "libsgwrap_thread.so",
    'time': "libsgwrap_time.so",
    'process': "libsgwrap_process.so",
}

def list_so_files_in_paths(paths):
    '''
    Returns a list of all .so files in a list of paths.
    '''
    so_files = list()
    for path in paths:
        so_files = so_files + glob.glob(path + '/*.so*')
    return so_files

def is_lib_in_libs(lib_filename, libs):
    '''
    Returns whether one elemenet of libs finishes with lib_filename.
    '''
    for lib in libs:
        if lib.endswith(lib_filename):
            return True
    return False

def check_loadable_libs():
    '''
    Check whether all sgwrap libs can be LD_PRELOADed in current environment.

    Throws an EnvironmentError on failure.
    '''
    missing_lib = False
    loadable_dynamic_libs_paths = os.environ['LD_LIBRARY_PATH'].split(':')
    loadable_dynamic_libs = list_so_files_in_paths(loadable_dynamic_libs_paths)
    for lib_name, lib_file in SGWRAP_LIBS.items():
        if not is_lib_in_libs(lib_file, loadable_dynamic_libs):
            print(f"sgwrap's {lib_name} library does not seem to be loadable: '{lib_file}' cannot be found in any directory listed in LD_LIBRARY_PATH")
            missing_lib = True

    if missing_lib:
        print('Aborting launch as some sgwrap libraries do not seem loadable.',
              'Here are the directories listed in LD_LIBRARY_PATH:',
              '\n'.join(['- ' + p for p in loadable_dynamic_libs_paths]),
              sep='\n',
        )
        raise EnvironmentError('Aborting launch as some sgwrap libraries do not seem loadable')

def launch(command_line, libs_to_preload, sw_dir=None, sw_iface=None, sw_dbg_lvl=None):
    '''
    Launch command_line by LD_PRELOADing a set of sgwrap libaries.

    This is done by 1. generating a temporary bash script 2. executing it.
    '''
    sw_dir = '/tmp/sgwrap' if sw_dir is None else sw_dir
    sw_iface = 10 if sw_iface is None else sw_iface
    sw_dbg_lvl = 0 if sw_dbg_lvl is None else sw_dbg_lvl

    # Generate LD_PRELOAD
    ld_preload = ':'.join(SGWRAP_LIBS[lib] for lib in libs_to_preload)

    # Create temporary script
    tmp_fd, tmp_path = tempfile.mkstemp(text=True)
    with os.fdopen(tmp_fd, 'w') as tmp_file:
        script = f"""\
export SOCKET_WRAPPER_DIR="{sw_dir}"
export SOCKET_WRAPPER_DEFAULT_IFACE={sw_iface}
export SOCKET_WRAPPER_DEBUGLEVEL={sw_dbg_lvl}
export LD_PRELOAD="{ld_preload}"

{command_line}
"""
        tmp_file.write(script)

    os.system(f"bash -eu {tmp_path}")
    # process should be managed correctly, I'm just putting back putting back
    # a os.system() call instead because the commented code fails.
    #
    # # Execute the script with bash
    # process = subprocess.run(['bash', '-eu', tmp_path],
    #                          stdout=subprocess.PIPE,
    #                          stderr=subprocess.STDOUT,
    #                          universal_newlines=True,
    #                          preexec_fn=lambda: os.setpgid(0, 0),
    # )

    # # Clean temporary script
    # os.remove(tmp_path)

    # # Checks execution success
    # if process.returncode != 0:
    #     print(f"Command execution failed (returncode={process.returncode})")
    #     print(f"--- begin of command output ---\n{process.stdout}\n--- end of command output ---")
    #     raise RuntimeError(f"Command execution failed (returncode={process.returncode})")

def main():
    '''
    Script entry point.
    '''
    arguments = docopt(__doc__, version='0.1.0')

    # Determine which libs should be loaded according to input parameters.
    libs_to_preload = ['init']
    for arg in arguments:
        for searched_arg in set(SGWRAP_LIBS.keys()) - {'init'}:
            if arg == "--" + searched_arg and arguments[arg] == 1:
                libs_to_preload.append(searched_arg)

    # Launch the process
    launch(" ".join(arguments["<command-line>"]), libs_to_preload,
           sw_dir=arguments["--sw-dir"],
           sw_iface=arguments["--sw-iface"],
           sw_dbg_lvl=arguments["--sw-dbg-lvl"],
    )

if __name__ == '__main__':
    main()
