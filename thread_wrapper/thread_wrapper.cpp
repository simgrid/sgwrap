// dl* functions
#include <dlfcn.h>
#include <stdio.h>
// RSG
#include <librsg.hpp>
#include <librsg/assert.hpp>

// Fork types (pid_t)
#include <sys/types.h>
// Fork
#include <unistd.h>
#include <string.h>
#include <unordered_map>

thread_local bool interception = false;

std::unordered_map<pthread_mutex_t*, rsg::MutexPtr> mutexes;

// Internal function loading a symbol from the libc and returning the `fn_name`
static void *_bind_symbol(const char *fn_name) {
  void *handle;
  void *func;

  // open the libc
  handle = dlopen("libpthread.so.0", RTLD_LAZY);

  // find and bind the function
  func = dlsym(handle, fn_name);
  if (func == NULL) {
    // TODO print dlerror
    exit(-1);
  }
  return func;
}

static void init_thrread_wrapper(void) __attribute__((constructor));
void init_thrread_wrapper(void)
{
  interception = true;
}

// typedefs to facilitate the declaration of the libc functions
typedef int (*__libc_pthread_create)(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg);
typedef int (*__libc_pthread_detach)(pthread_t *thread);
typedef int (*__libc_pthread_join)(pthread_t thread, void **reval);

typedef int (*__libc_pthread_mutex_init)(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr);
typedef int (*__libc_pthread_mutex_lock)(pthread_mutex_t *mutex);
typedef int (*__libc_pthread_mutex_unlock)(pthread_mutex_t *mutex);
typedef int (*__libc_pthread_mutex_trylock)(pthread_mutex_t *mutex);

// global variables holding the ptr to the execv function
static __libc_pthread_create ptrpthread_create;
static __libc_pthread_detach ptrpthread_detach;
static __libc_pthread_join ptrpthread_join;

static __libc_pthread_mutex_init ptrpthread_mutex_init;
static __libc_pthread_mutex_lock  ptrpthread_mutex_lock;
static __libc_pthread_mutex_unlock ptrpthread_mutex_unlock;
static __libc_pthread_mutex_trylock ptrpthread_mutex_trylock;

// Macro that bind a symbol if it is not already done.
// The macro will look at a variable named ptr`fn_name`.
#define _bind_symbol_libc(sym_name)                                            \
  if (ptr##sym_name == NULL) {                                                 \
    ptr##sym_name = (__libc_##sym_name)_bind_symbol(#sym_name);                \
  }

static int libc_pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg) {
  _bind_symbol_libc(pthread_create);
  return ptrpthread_create(thread, attr, start_routine, arg);
}
static int libc_pthread_detach(pthread_t *thread) {
  _bind_symbol_libc(pthread_detach);
  return ptrpthread_detach(thread);
}
static int libc_pthread_join(pthread_t thread, void **retval){
  _bind_symbol_libc(pthread_join);
  return ptrpthread_join(thread, retval);
}
static int libc_pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr) {
  _bind_symbol_libc(pthread_mutex_init);
  return ptrpthread_mutex_init(mutex, attr);;
}
static int libc_pthread_mutex_destroy(pthread_mutex_t *mutex) {
  return 0;
}
static int libc_pthread_mutex_lock(pthread_mutex_t *mutex) {
  _bind_symbol_libc(pthread_mutex_lock);
  return ptrpthread_mutex_lock(mutex);
}
static int libc_pthread_mutex_unlock(pthread_mutex_t *mutex) {
  _bind_symbol_libc(pthread_mutex_unlock);
  return ptrpthread_mutex_unlock(mutex);
}
static int libc_pthread_mutex_trylock(pthread_mutex_t *mutex) {
  _bind_symbol_libc(pthread_mutex_trylock);
  return ptrpthread_mutex_trylock(mutex);
}


void before_rsg_connect(void *arg) {
  if(arg != NULL) {
    //Socket wrapper not enabled
    unsetenv("SOCKET_WRAPPER_DIR");
  }
  interception = true;
}

void after_rsg_connect(void *arg) {
  char *sw_dir = (char *) arg;
  if(sw_dir != NULL) {
    printf("Activate socket wrapper: %s\n", arg);
    //Socket wrapper not enabled
    // Enable socket_wrapper interception
    setenv("SOCKET_WRAPPER_DIR", sw_dir, 1);
  }
}

// put this function into the extern block causes the build to fail
int pthread_cond_init(pthread_cond_t *cond) {
  return 0;
}

extern "C" {
int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg) {
  if (interception == true) {
    // disable socket_wrapper, so the child can connect to rsg
    char *initial_value = getenv("SOCKET_WRAPPER_DIR");
    char *sw_dir = NULL;

    if(initial_value != NULL) {
      sw_dir = strdup(initial_value);
    }

    // Calling the RSG fork, that will create a new actor
    // INTO a new system process.
    auto actor = rsg::Actor::self();
    auto my_host = actor->get_host();

    std::string child_name = std::to_string(actor->get_pid()) + std::string(":thread");

    interception = false;
    auto thread_actor = rsg::Actor::create(child_name, my_host, start_routine, arg,
        before_rsg_connect, (void*)sw_dir, after_rsg_connect, (void*)sw_dir);
    interception = true;

    sw_dir = NULL;
    *thread = (pthread_t) thread_actor->get_pid();
    return 0;

  } else {
    // If the environment variable `SGWRAP_NEW_THREAD` is set, it means that we are intercepting
    // an internal call to pthread_create, we call the original function from the libc.
    int res = libc_pthread_create(thread, attr, start_routine, arg);
    return res;
  }
}

int pthread_detach(pthread_t thread) {
  return 0;
}

int pthread_join(pthread_t thread, void **retval) {
  return 0;
}

int pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr) {
  if(!interception){
    return 0;
  }

  int type;
  if (attr != NULL) {
    pthread_mutexattr_gettype(attr, &type);
    switch(type) {
      // DEFAULT being set to NORMAL
      // case PTHREAD_MUTEX_DEFAULT:
      //   printf("Mutex: %s\n", "PTHREAD_MUTEX_DEFAULT");
      //   break;
      case PTHREAD_MUTEX_NORMAL:
        printf("Mutex: %s\n", "PTHREAD_MUTEX_NORMAL");
        break;
      case PTHREAD_MUTEX_ERRORCHECK:
        printf("Mutex: %s\n", "PTHREAD_MUTEX_ERRORCHECK");
        break;
      case PTHREAD_MUTEX_RECURSIVE:
        printf("Mutex: %s\n", "PTHREAD_MUTEX_RECURSIVE");
        break;
      default :
      {
        printf("undefined mutex type");
        exit(-1);
      }
    }
  }

  auto rsg_mutex = rsg::Mutex::create();
  mutexes.insert({mutex, rsg_mutex});

  return 0;
}

int pthread_mutex_destroy(pthread_mutex_t *mutex) {
  if(!interception){
    return libc_pthread_mutex_destroy(mutex);
  }

  mutexes.erase(mutex);
  return 0;
}

int pthread_mutex_lock(pthread_mutex_t *mutex) {
  if(!interception){
    return libc_pthread_mutex_lock(mutex);
  }
  mutexes.at(mutex)->lock();
  return 0;
}

int pthread_mutex_unlock(pthread_mutex_t *mutex) {
  if(!interception){
    return libc_pthread_mutex_unlock(mutex);
  }

  mutexes.at(mutex)->unlock();
  return 0;
}

int pthread_mutex_trylock(pthread_mutex_t *mutex) {
  if(!interception){
    return libc_pthread_mutex_trylock(mutex);
  }

  return mutexes.at(mutex)->try_lock();
}

long syscall(long number, ...) {
  printf("syscall Calling %s (IDK what to do) %s\n", __func__);
  return 0;
}

int pthread_cond_signal(pthread_cond_t *cond) {
  return 0;
}

int pthread_cond_broadcast(pthread_cond_t *cond) {
  rsg::this_actor::sleep_for(0.1);
  return 0;
}

int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex) {
  return 0;
}

int pthread_cond_timedwait(pthread_cond_t *cond, pthread_mutex_t *mutex, const struct timespec *abstime) {
  return 0;
}

int pthread_cond_destroy(pthread_cond_t *cond) {
  return 0;
}

}
