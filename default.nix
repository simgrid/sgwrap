{ kapack ? import
    (fetchTarball "https://github.com/oar-team/nur-kapack/archive/master.tar.gz")
  {}
, rsg ? kapack.rsg-master
}:

let
  pkgs = kapack.pkgs;
  packages = rec {
    sgwrap = pkgs.stdenv.mkDerivation rec {
      pname = "sgwrap";
      version = "0.1.0-git";

      # filter source files to avoid useless rebuilds
      src = pkgs.lib.sourceByRegex ./. [
        "^meson\.build"
        "^process_wrapper"
        "^process_wrapper/meson\.build"
        "^process_wrapper/.*\.cpp"
        "^sgwrap_init"
        "^sgwrap_init/meson\.build"
        "^sgwrap_init/.*\.cpp"
        "^thread_wrapper"
        "^thread_wrapper/meson\.build"
        "^thread_wrapper/.*\.cpp"
        "^time_wrapper"
        "^time_wrapper/meson\.build"
        "^time_wrapper/.*\.cpp"
      ];

      nativeBuildInputs = with pkgs; [ meson pkgconfig ninja ];
      buildInputs = [ rsg ];
    };

    socket_wrapper = pkgs.stdenv.mkDerivation rec {
      pname = "sgwrap-socket-wrapper";
      version = "0.1.0-git";

      src = pkgs.lib.sourceByRegex ./socket_wrapper [
        "^CMakeLists\.txt"
        "^README\.md"
        "^LICENSE"
        "^.*\.cmake.*"
        "^cmake"
        "^cmake/.*\.cmake"
        "^cmake/Modules"
        "^cmake/Modules/.*\.cmake"
        "^doc"
        "^doc/.*"
        "^src"
        "^src/CMakeLists\.txt"
        "^src/.*\.cpp"
      ];
      nativeBuildInputs = with pkgs; [ cmake pkgconfig ];
      buildInputs = [ rsg ];
    };

    sgwrap-launcher = pkgs.python3Packages.buildPythonPackage {
      name = "sgwrap-launcher";
      version = "0.1.0";
      propagatedBuildInputs = with pkgs.python3Packages; [
        docopt
      ];
      src = ./launcher;
    };

    test-shell = pkgs.mkShell rec {
      buildInputs = [
        rsg
        sgwrap
        socket_wrapper
        sgwrap-launcher
      ];

      shellHook = ''
        export LD_LIBRARY_PATH="${sgwrap}/lib:${socket_wrapper}/lib:$LD_LIBRARY_PATH"
      '';
    };

    # Dev shell
    # Activate with nix-shell /path/to/sgwrap -A shell
    shell = pkgs.mkShell rec {
      buildInputs = with pkgs; [
        kapack.simgrid
        rsg
        meson
        ninja
        cmake
        pkgconfig
        valgrind
        (python3.withPackages (ps: with ps; with python3Packages; [ docopt ]))
      ];

      # emplacement for sgwrap libraries
      SGWRAP_SOCKET_WRAPPER="socket_wrapper/b/src";
      SGWRAP_LIB_FOLDER="b";
    };

  };
in
  packages
